def main():
    a=[1,0,10,2,67,4,9,34]
    InsertionSort(a,len(a))
    for i in a:
        print(i)

def InsertionSort(a,l):
    for i in range(0,l):
        item=a[i]
        for j in range(i-1,-1,-1):
            if a[j]>item:
                a[j+1]=a[j]
                a[j]=item

if __name__=="__main__":
    main()