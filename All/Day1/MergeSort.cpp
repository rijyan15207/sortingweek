#include<iostream>
using namespace std;
//Prototypes
void Divide(int*,int,int);
void Conquer(int*,int,int,int);
//Main function
int main()
{
//Declaring an array of size 8 and initializing values
int a[8]={108,15,50,4,8,42,23,16};
//Calling function divide passing array,its first index and last index
Divide(a,0,7);
//Printing the sorted result
for(int i=0;i<8;i++)
{
    cout<<endl<<a[i];
}
}
//Function that will split list in half until list become atom
void Divide(int* a,int low,int high)
{
int mid;
//Calculating whether there is more than 1 element or not
if(low<high)
{
//calculating the middle index of list
mid=(low+high)/2;
//Calling divide again passing first half of list
Divide(a,low,mid);
//Calling divide function again passing remaining last half of list
Divide(a,mid+1,high);
//calling conquer function passing the list
Conquer(a,low,mid,high);
}
}
//It will merge list on sorted order
void Conquer(int *a,int low,int mid,int high)
{
int i,j,k,c[10];
//initializing i to low to start reading value from first half
i=low;
//initializing j to mid+1 to start reading value from remaining half
j=mid+1;
//initializing k to low because we have to change the element in original array
//only from this point
k=0;
//checking whether any of our halves of list is fully visited
while(i<=mid && j<=high)
{
//checking which element is less between first element of first half and
//first element of second half
if(a[i]<a[j])
{
//if first element of first half is smaller then initialize that element to
//our temporary list and increase both k and i by 1
c[k]=a[i];
k++;
i++;
}
else
{
//if first element of second half is smaller then initialize that element to
//our temporary list and increase k and j by 1
c[k]=a[j];
k++;
j++;
}
}
//Now check if any element is left to visit in list first half if yes then assign
//the value serially to temp list because that half will already be sorted and
//next half in already traversed
while(i<=mid)
{
    c[k]=a[i];
    k++;
    i++;
}
//Now check if any element is left to visit in list second half if yes then
//assign the value serially to temp list because that half will already be
//sorted and next half is already traversed
while(j<=high)
{
    c[k]=a[j];
    k++;
    j++;
}
//Now change the original list with our sorted list
for(int m=low,n=0;m<k+low;m++,n++)
{
    a[m]=c[n];
}
}

