def main():
    a=[1,100,2,4,0,234,34,5,3,7]
    a=HeapSort(a,len(a)-1)
    for  i in a:
        print(i)

def HeapSort(a,l):
    emp=[]
    while l>=0:
        mid=l//2
        for i in range(mid,-1,-1):
            left=2*mid+1
            right=2*mid+2
            if left<=l:
                if a[left]<a[mid]:
                    a[mid],a[left]=a[left],a[mid]
            if right<=l:
                if a[right]<a[mid]:
                    a[mid],a[right]=a[right],a[mid]
            mid-=1
        emp.append(a[0])
        a[0]=a[l]
        l-=1

    return emp

if __name__=="__main__":
    main()