def main():
    a=[1,2,0,10,6,90,12,76]
    SelectionSort(a,len(a))
    for i in a:
        print(i)

def SelectionSort(a,n):
    for i in range(0,n):
        for j in range(i+1,n):
            if a[i]>a[j]:
                a[i],a[j]=a[j],a[i]

if __name__=="__main__":
    main()