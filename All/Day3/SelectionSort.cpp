#include<iostream>
using namespace std;
void Sort(int*,int);
int  main()
{
//declaring and initializing an array of elements
int a[8]={1,9,2,10,98,0,4,8};
//calling the sorting function passing array and length of array
Sort(a,8);
//Printing the elements
for(int i=0;i<8;i++)
    cout<<"\n"<<a[i];
return 0;
}
void Sort(int *a,int n)
{
//Traversing through each element in complete list
for(int i=0;i<n;i++)
{
    //Traversing again through the entire list from position i
    for(int j=i+1;j<n;j++)
    {
        //comparing a[i] element with all other elements after that to find out whether it is greater or not
        if(a[i]>a[j])
        {
            //If greater than we are swapping both the elements
            int temp=a[i];
            a[i]=a[j];
            a[j]=temp;
        }
    }
}
}
