#include<stdio.h>
void Swap(int *a,int *b)
{
    int temp=*a;
    *a=*b;
    *b=temp;
}
int main()
{
int a[8]={1,3,4,2,0,5,90,78};
int len=8;
int pos=0;
while(pos<len)
{
    if(pos==0 || a[pos]>a[pos-1])
        pos++;
    else
    {
        Swap(&a[pos],&a[pos-1]);
        pos--;
    }
}
for(int i=0;i<len;i++)
{
    printf("%d ",a[i]);
}

return 0;
}
